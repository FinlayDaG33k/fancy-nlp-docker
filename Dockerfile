FROM mhart/alpine-node:10

# Install dependencies
RUN apk add --no-cache git

# Create app dir
RUN mkdir /app
WORKDIR /app

# Install fancy-nlp
RUN git clone https://github.com/fancy-flashcard/fancy-nlp.git /app \
    && npm install \
    && mkdir /app/operational-data \
    && echo "[]" > /app/operational-data/messages.json

CMD ["node", "fancy-nlp-server.js", "8080"]